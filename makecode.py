from microbit import *

HAPPY = 0
SAD   = 1

def show_image (img):
  if img == HAPPY:
    display.show(Image.HAPPY)
  elif img == SAD:
    display.show(Image.SAD)
